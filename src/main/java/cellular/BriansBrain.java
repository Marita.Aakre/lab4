package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
	
	IGrid currentGeneration;
	
	public BriansBrain(int rows, int columns) {
		currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
		initializeCells();
	}

	@Override
	public CellState getCellState(int row, int column) {
		return currentGeneration.get(row, column);
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}	
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		for (int row = 0; row < numberOfRows(); row++) {
			for (int col = 0; col < numberOfColumns(); col++) {
				nextGeneration.set(row, col, getNextCell(row, col));
			}
		}
	}

	@Override
	public CellState getNextCell(int row, int col) {
		CellState cellState = getCellState(row, col);
		int aliveNeighbors = countNeighbors(row, col, CellState.ALIVE);
		
		if(cellState.equals(CellState.ALIVE)) {
			return CellState.DYING;
		} else if(cellState.equals(CellState.DYING)) {
			return CellState.DEAD;
		}
		else if(cellState.equals(CellState.DEAD)  && aliveNeighbors == 2) {
			return CellState.ALIVE;
		} else {
			return CellState.DEAD;
		}
	}
	
	private int countNeighbors(int row, int col, CellState state) {
		int neighborCount = 0;
		
		for (int r = row-1; r <= row+1; r++) {
			for (int c = col-1; c <= col+1; c++) {
				if (r >= currentGeneration.numRows() || c >= currentGeneration.numColumns() || r < 0 || c < 0) {
					continue;
				}
				if (r == row && c == col) {
					continue;
				} else if (state == currentGeneration.get(r, c)) {
					neighborCount++;
				}
			}
		}		
		return neighborCount;
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
	
	
	

}
